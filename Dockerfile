FROM openjdk:8
MAINTAINER Jacob Cabral <jacob-cabral@live.com>

ADD target/file-store.jar /usr/share/file-store/file-store.jar

EXPOSE 8080

CMD [ "java", "-jar", "/usr/share/file-store/file-store.jar" ]