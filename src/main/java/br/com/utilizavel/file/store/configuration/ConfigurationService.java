package br.com.utilizavel.file.store.configuration;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class ConfigurationService {

  @Value("${file-store.path}")
  private String path;

  @Value("${spring.servlet.multipart.max-file-size}")
  private String maxFileSize;

  public String getPath() {
    return path;
  }

  public String getMaxFileSize() {
    return maxFileSize;
  }
}
