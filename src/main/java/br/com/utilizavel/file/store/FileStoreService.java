package br.com.utilizavel.file.store;

import br.com.utilizavel.file.store.configuration.ConfigurationService;
import br.com.utilizavel.file.store.exception.IllegalFileException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static java.nio.file.StandardCopyOption.REPLACE_EXISTING;

@Service
public class FileStoreService {

  private final Path fileStorePath;

  @Autowired
  public FileStoreService(final ConfigurationService configurationService) {
    super();
    Optional<String> optionalPath = Optional.ofNullable(configurationService.getPath());
    File dir = new File(optionalPath.orElseThrow(() -> new IllegalStateException("O caminho de armazenamento dos arquivos não foi definido.")));

    if (dir.isDirectory()) fileStorePath = dir.toPath();
    else throw new IllegalStateException("O caminho de armazenamento dos arquivos não corresponde a um diretório.");
  }

  public List<File> list(final int first, final int quantity) throws IOException {
    if (first < 1) throw new IllegalFileException("O nº de ordem do primeiro arquivo não pode ser menor que 1.");

    if (quantity < 1) throw new IllegalFileException("A quantidade de arquivos não pode ser menor do que 1.");

    final int size = count();

    if (first > size) throw new IllegalFileException(String.format("O nº de ordem do primeiro arquivo não pode ser maior que o tamanho da lista de arquivos (%d).", size));

    final int firstIndexInclusive = first - 1;
    final int lastIndexExclusive = Math.min(firstIndexInclusive + quantity, size);

    return Collections.unmodifiableList(Files.list(fileStorePath).sorted().map(Path::toFile).collect(Collectors.toList()).subList(firstIndexInclusive, lastIndexExclusive));
  }

  public Optional<File> find(final String filename) throws IOException {
    Optional.ofNullable(filename).orElseThrow(() -> new IllegalFileException("O nome do arquivo não pode ser nulo."));
    Optional.of(filename).filter(f -> !f.isEmpty()).orElseThrow(() -> new IllegalFileException("O nome do arquivo não pode ser vazio."));

    return Files.list(fileStorePath).map(Path::toFile).filter(f -> f.getName().equalsIgnoreCase(filename)).findFirst();
  }

  public void save(final File file) throws IOException {
    Optional.ofNullable(file).orElseThrow(() -> new IllegalFileException("O arquivo não pode ser nulo."));
    Optional.of(file).filter(File::exists).orElseThrow(() -> new IllegalFileException("O arquivo não existe."));

    final File newFile = new File(fileStorePath.toFile(), file.getName());
    Files.copy(file.toPath(), newFile.toPath(), REPLACE_EXISTING);
  }

  public void delete(final String filename) throws IOException {
    Optional.ofNullable(filename).orElseThrow(() -> new IllegalFileException("O nome do arquivo não pode ser nulo."));
    Optional.of(filename).filter(f -> !f.isEmpty()).orElseThrow(() -> new IllegalFileException("O nome do arquivo não pode ser vazio."));
    Optional<File> optionalFile = find(filename);

    if (optionalFile.isPresent()) Files.deleteIfExists(optionalFile.get().toPath());
  }

  public int count() throws IOException {
    return (int) Files.list(fileStorePath).count();
  }
}