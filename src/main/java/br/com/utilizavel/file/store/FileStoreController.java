package br.com.utilizavel.file.store;

import br.com.utilizavel.file.store.exception.IllegalFileException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.MimeTypeUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.*;
import java.nio.file.Files;
import java.util.Collections;
import java.util.Optional;
import java.util.stream.Stream;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;
import static org.springframework.http.HttpStatus.CREATED;

@RestController
@RequestMapping("/api/v1/file-store/files")
public class FileStoreController {

  private final FileStoreService service;

  @Autowired
  public FileStoreController(FileStoreService service) {
    super();
    this.service = service;
  }

  @GetMapping
  public ResponseEntity<Stream<Link>> list(@RequestParam(name = "first", defaultValue = "1") final int first, @RequestParam(name = "quantity", defaultValue = "10") final int quantity) throws IOException {
    return ResponseEntity.ok().body(service.list(first, quantity).stream().map(f -> linkTo(methodOn(FileStoreController.class).find(f.getName())).withRel("file")));
  }

  @GetMapping("/{filename}")
  public ResponseEntity<Resource> find(@PathVariable("filename") String filename) {
    try {
      final Optional<File> optionalFile = service.find(filename);

      if (optionalFile.isEmpty()) return ResponseEntity.notFound().build();

      final File file = optionalFile.get();
      return ResponseEntity.ok()
        .header(HttpHeaders.CONTENT_DISPOSITION, "attachment", "filename=".concat(file.getName()))
        .contentLength(file.length())
        .contentType(probeContentType(file)).body(new ByteArrayResource(Files.readAllBytes(file.toPath())));
    } catch (IOException exception) {
      throw new IllegalFileException(String.format("Ocorreu erro ao buscar o arquivo %s.", filename), exception);
    }
  }

  @PostMapping
  public ResponseEntity<Resource> save(@RequestParam("file") MultipartFile multipartFile) throws IOException {
    File newFile = new File(System.getProperty("java.io.tmpdir"), multipartFile.getOriginalFilename());
    ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
    byteArrayOutputStream.writeBytes(multipartFile.getBytes());

    try (OutputStream outputStream = new FileOutputStream(newFile)) {
      byteArrayOutputStream.writeTo(outputStream);
    }

    service.save(newFile);

    HttpHeaders headers = new HttpHeaders();
    headers.setLocation(linkTo(methodOn(FileStoreController.class).find(newFile.getName())).withRel("file").toUri());

    return ResponseEntity.status(CREATED).headers(HttpHeaders.readOnlyHttpHeaders(headers)).build();
  }

  @DeleteMapping("/{filename}")
  public ResponseEntity<?> delete(@PathVariable("filename") String filename) throws IOException {
    Optional<File> optionalFile = service.find(filename);

    if (optionalFile.isPresent()) Files.deleteIfExists(optionalFile.get().toPath());

    return ResponseEntity.noContent().build();
  }

  @GetMapping("/size")
  public ResponseEntity<?> count() throws IOException {
    return ResponseEntity.ok(service.count());
  }

  private MediaType probeContentType(File file) throws IOException {
    final String defaultMimeType = MimeTypeUtils.toString(Collections.singleton(MimeTypeUtils.APPLICATION_OCTET_STREAM));
    final String mimeType = Files.probeContentType(file.toPath());
    return MediaType.asMediaType(MimeTypeUtils.parseMimeType(Optional.ofNullable(mimeType).orElse(defaultMimeType)));
  }
}