package br.com.utilizavel.file.store.exception;

import br.com.utilizavel.file.store.configuration.ConfigurationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.multipart.MaxUploadSizeExceededException;
import org.springframework.web.multipart.MultipartException;

import java.io.IOException;

import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR;

@ControllerAdvice
public class ExceptionHandlingController {

  private final ConfigurationService configurationService;

  @Autowired
  public ExceptionHandlingController(ConfigurationService configurationService) {
    this.configurationService = configurationService;
  }

  @ExceptionHandler(MaxUploadSizeExceededException.class)
  public ResponseEntity<?> handleMaxUploadSizeExceededException(MaxUploadSizeExceededException exception) {
    exception.printStackTrace();
    return ResponseEntity.badRequest().body(String.format("O tamanho máximo de upload foi excedido (%s).", configurationService.getMaxFileSize()));
  }

  @ExceptionHandler(IOException.class)
  @ResponseStatus(code = INTERNAL_SERVER_ERROR, reason = "Ocorreu erro relacionado ao armazenamento de arquivo.")
  public void handleIOException(Exception exception) {
    exception.printStackTrace();
  }

  @ExceptionHandler(MultipartException.class)
  @ResponseStatus(code = BAD_REQUEST, reason = "A requisição não é multipart.")
  public void handleMultipartException(Exception exception) {
    exception.printStackTrace();
  }
}