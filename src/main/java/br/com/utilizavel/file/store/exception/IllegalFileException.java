package br.com.utilizavel.file.store.exception;

import org.springframework.web.bind.annotation.ResponseStatus;

import static org.springframework.http.HttpStatus.BAD_REQUEST;

@ResponseStatus(BAD_REQUEST)
public class IllegalFileException extends IllegalArgumentException {
  public IllegalFileException() {
    super();
  }

  public IllegalFileException(String message) {
    super(message);
  }

  public IllegalFileException(String message, Throwable cause) {
    super(message, cause);
  }

  public IllegalFileException(Throwable cause) {
    super(cause);
  }
}
