package br.com.utilizavel.file.store;

import br.com.utilizavel.file.store.exception.IllegalFileException;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static org.assertj.core.api.Assertions.*;

@SpringBootTest
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class FileStoreServiceTest {

  public static final String FILE_STORE_PATH_VALUE = "src/test/resources/.files";
  public static final List<String> EXPECTED_FILENAMES = IntStream.rangeClosed(1, 35).mapToObj(n -> String.format("file-%02d.xlsb", n)).sorted().collect(Collectors.toList());

  private final FileStoreService service;

  @Autowired
  public FileStoreServiceTest(FileStoreService service) {
    this.service = service;
  }

  @Test
  @Order(1)
  public void testContext() {
    assertThat(service).isNotNull();
  }

  @Test
  @Order(2)
  public void testList() throws IOException {
    final int first = 1;
    final int quantity = 10;
    final List<File> files = service.list(first, quantity);

    assertThat(files).isNotNull();
    assertThat(files).isNotEmpty();
    assertThat(files).hasSize(quantity);
    assertThat(files.stream().map(File::getName)).containsExactlyElementsOf(EXPECTED_FILENAMES.subList(first - 1, quantity));
  }

  @Test
  @Order(2)
  public void testListLast5() throws IOException {
    final int first = 31;
    final int quantity = 10;
    final List<File> files = service.list(first, quantity);

    assertThat(files).isNotNull();
    assertThat(files).isNotEmpty();
    assertThat(files).hasSize(5);
    assertThat(files.stream().map(File::getName)).containsExactlyElementsOf(EXPECTED_FILENAMES.subList(first - 1, EXPECTED_FILENAMES.size()));
  }

  @Test
  @Order(2)
  public void testListWithNegativeIndex() {
    assertThatExceptionOfType(IllegalFileException.class).isThrownBy(() -> service.list(0, 10)).withMessage("O nº de ordem do primeiro arquivo não pode ser menor que 1.");
  }

  @Test
  @Order(2)
  public void testListWithIndexGreaterThanSize() {
    assertThatExceptionOfType(IllegalFileException.class).isThrownBy(() -> service.list(36, 10)).withMessage("O nº de ordem do primeiro arquivo não pode ser maior que o tamanho da lista de arquivos (35).");
  }

  @Test
  @Order(2)
  public void testListWithQuantityLessThan1() {
    assertThatExceptionOfType(IllegalFileException.class).isThrownBy(() -> service.list(1, 0)).withMessage("A quantidade de arquivos não pode ser menor do que 1.");
  }

  @Test
  @Order(2)
  public void testFind() throws IOException {
    final String filename = "file-01.xlsb";
    final Optional<File> optionalFile = service.find(filename);

    assertThat(optionalFile).isPresent();
    assertThat(optionalFile.get().getName()).isEqualTo(filename);
  }

  @Test
  @Order(2)
  public void testFindNonexistentFile() throws IOException {
    final Optional<File> optionalFile = service.find("nonexistent.xlsb");
    assertThat(optionalFile).isNotPresent();
  }

  @Test
  @Order(2)
  public void testFindWithNullFilename() {
    assertThatExceptionOfType(IllegalFileException.class).isThrownBy(() -> service.find(null)).withMessage("O nome do arquivo não pode ser nulo.");
  }

  @Test
  @Order(2)
  public void testFindWithEmptyFilename() {
    assertThatExceptionOfType(IllegalFileException.class).isThrownBy(() -> service.find("")).withMessage("O nome do arquivo não pode ser vazio.");
  }

  @Test
  @Order(3)
  public void testSave() throws IOException {
    final String newFilename = "new.xlsb";
    final File newFile = new File("src/test/resources", newFilename);
    final int oldQuantity = service.count();

    assertThat(service.find(newFilename)).isNotPresent();

    service.save(newFile);

    assertThat(service.find(newFilename)).isPresent();

    final int first = 1;
    final List<File> files = service.list(first, service.count());

    assertThat(files.stream().map(File::getName)).contains(newFile.getName());
    assertThat(files).hasSizeGreaterThan(oldQuantity);
  }

  @Test
  @Order(2)
  public void testSaveNullFile() {
    assertThatExceptionOfType(IllegalFileException.class).isThrownBy(() -> service.save(null)).withMessage("O arquivo não pode ser nulo.");
  }

  @Test
  @Order(2)
  public void testSaveNonexistentFile() {
    assertThatExceptionOfType(IllegalFileException.class).isThrownBy(() -> service.save(new File("nonexistent.xlsb"))).withMessage("O arquivo não existe.");
  }

  @Test
  @Order(4)
  public void testDelete() throws IOException {
    final String filename = "new.xlsb";
    final int oldQuantity = service.count();

    final Optional<File> optionalFile = service.find(filename);

    assertThat(optionalFile).isPresent();

    final File deletedFile = optionalFile.get();

    assertThat(deletedFile).hasName(filename);
    assertThat(deletedFile).exists();

    service.delete(filename);

    assertThat(deletedFile).doesNotExist();
    assertThat(service.find(filename)).isNotPresent();

    final int first = 1;
    final List<File> files = service.list(first, service.count());

    assertThat(files).doesNotContain(deletedFile);
    assertThat(files).hasSizeLessThan(oldQuantity);
  }

  @Test
  @Order(2)
  public void testDeleteNonexistentFile() throws IOException {
    final String filename = "nonexistent.xlsb";
    final File nonexistentFile = new File(filename);
    final int first = 1;
    final int quantity = service.count();
    final List<File> files = service.list(first, quantity);
    final Optional<File> optionalFile = service.find(filename);

    assertThat(optionalFile).isNotPresent();
    assertThat(nonexistentFile).doesNotExist();
    assertThat(files).doesNotContain(nonexistentFile);

    service.delete(filename);

    assertThat(files).hasSize(quantity);
  }

  @Test
  @Order(2)
  public void testDeleteWithNullFilename() {
    assertThatExceptionOfType(IllegalFileException.class).isThrownBy(() -> service.delete(null)).withMessage("O nome do arquivo não pode ser nulo.");
  }

  @Test
  @Order(2)
  public void testDeleteWithEmptyFilename() {
    assertThatExceptionOfType(IllegalFileException.class).isThrownBy(() -> service.find("")).withMessage("O nome do arquivo não pode ser vazio.");
  }

  @Test
  @Order(2)
  public void testCount() throws IOException {
    final Path path = new File(FILE_STORE_PATH_VALUE).toPath();
    final int filesCount = (int) Files.list(path).count();

    assertThat(service.count()).isEqualTo(filesCount);
  }
}